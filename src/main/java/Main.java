import lombok.extern.slf4j.Slf4j;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.Locale;

/**
 * @author Another
 *         date: 20.12.16.
 */

@Slf4j
public class Main { /// Нужен только для понимания работы с java.time

	private static final String STANDARD_DATE_FORMAT = "dd-MM-yyyy";
	private static final Long MONTH_SHIFT = 2L;

	private static String localDateToString(LocalDate date, String dateFormat) {
		return date.format(DateTimeFormatter.ofPattern(dateFormat));
	}

	public static void main(String[] args) {
		LocalDate date = LocalDate.now();
		log.info("Date instance: {}", localDateToString(date, STANDARD_DATE_FORMAT));

		date = date.plusMonths(MONTH_SHIFT);
		log.info("Date plus {} months: {}", MONTH_SHIFT, localDateToString(date, STANDARD_DATE_FORMAT));

		log.info("{}", DayOfWeek.SATURDAY.getValue());
		log.info("{}", DayOfWeek.SUNDAY.getValue());

		TemporalField fieldRu = WeekFields.of(Locale.getDefault()).dayOfWeek();
		LocalDate newLocalDateSaturday = date.with(fieldRu, DayOfWeek.SATURDAY.getValue());
		log.info("Date plus {} months: {}", MONTH_SHIFT, localDateToString(newLocalDateSaturday, STANDARD_DATE_FORMAT));

		LocalDate newLocalDateSunday = date.with(fieldRu, DayOfWeek.SUNDAY.getValue());
		log.info("Date plus {} months: {}", MONTH_SHIFT, localDateToString(newLocalDateSunday, STANDARD_DATE_FORMAT));
	}
}
