package ru.another;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecondWebHelperBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecondWebHelperBeApplication.class, args);
	}
}
