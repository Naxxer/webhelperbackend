package ru.another.business.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.another.business.dao.custom.IFoundsFlowTypeDaoCustom;
import ru.another.business.domain.FoundsFlowType;

/**
 * @author Anton.Ilin
 *         date: 02.12.16.
 */
@Transactional
public interface IFoundsFlowTypeDao extends CrudRepository<FoundsFlowType, Long>, IFoundsFlowTypeDaoCustom {

	FoundsFlowType findByCode(Long code);

	FoundsFlowType findByTitle(String title);
}
