package ru.another.business.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.another.business.dao.custom.ILedgerDaoCustom;
import ru.another.business.domain.Ledger;

/**
 * @author Anton.Ilin
 *         date: 02.12.16.
 */
@Transactional
public interface ILedgerDao extends CrudRepository<Ledger, Long>, ILedgerDaoCustom {

	Double fetchFoundsFlowAmount(String foundsFlowTypeTitle);
}
