package ru.another.business.dao.custom;

import ru.another.business.domain.FoundsFlowType;

/**
 * @author Another
 *         date: 05.01.17.
 *         
 * Name of interface must be {MainDaoInterfaceName}Custom
 */
public interface IFoundsFlowTypeDaoCustom {

	FoundsFlowType findByCode(Long code);

	FoundsFlowType findByTitle(String title);
}
