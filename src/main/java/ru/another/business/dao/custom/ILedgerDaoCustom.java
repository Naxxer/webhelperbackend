package ru.another.business.dao.custom;

/**
 * @author Another
 *         date: 05.01.17.
 *
 * Name of interface must be {MainDaoInterfaceName}Custom
 */

public interface ILedgerDaoCustom {

	Double fetchFoundsFlowAmount(String foundsFlowTypeTitle);
}
