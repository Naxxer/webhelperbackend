package ru.another.business.dao.implementation;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.another.business.dao.custom.IFoundsFlowTypeDaoCustom;
import ru.another.business.domain.FoundsFlowType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Another
 *         date: 05.01.17.
 */
@Repository
@Transactional
public class IFoundsFlowTypeDaoImpl implements IFoundsFlowTypeDaoCustom {

	@PersistenceContext
	private EntityManager entityManager;

	private Session getCurrentSession() {
		return entityManager.unwrap(Session.class);
	}

	@Override
	public FoundsFlowType findByCode(Long code) {
		Criteria criteria = getCurrentSession().createCriteria(FoundsFlowType.class)
				.add(Restrictions.eq("code", code));
		return (FoundsFlowType) criteria.uniqueResult();
	}

	@Override
	public FoundsFlowType findByTitle(String title) {
		Criteria criteria = getCurrentSession().createCriteria(FoundsFlowType.class)
				.add(Restrictions.eq("title", title));
		return (FoundsFlowType) criteria.uniqueResult();
	}
}
