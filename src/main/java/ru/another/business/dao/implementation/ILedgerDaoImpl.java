package ru.another.business.dao.implementation;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.another.business.dao.IFoundsFlowTypeDao;
import ru.another.business.dao.custom.ILedgerDaoCustom;
import ru.another.business.domain.FoundsFlowType;
import ru.another.business.domain.FoundsFlowType_;
import ru.another.business.domain.Ledger;
import ru.another.business.domain.Ledger_;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * @author Another
 *         date: 05.01.17.
 */
@Repository
@Transactional
public class ILedgerDaoImpl implements ILedgerDaoCustom {

    @PersistenceContext
    private EntityManager entityManager;

    private Session getCurrentSession() {
        return entityManager.unwrap(Session.class);
    }

    private final IFoundsFlowTypeDao dao;

    @Autowired
    public ILedgerDaoImpl(IFoundsFlowTypeDao dao) {
        this.dao = dao;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Double fetchFoundsFlowAmount(String foundsFlowTypeTitle) {
        Criteria incomeLedgerAmountCriteria = getCurrentSession().createCriteria(Ledger.class)
                .add(Restrictions.eq("foundsFlowsType.id", dao.findByTitle(foundsFlowTypeTitle).getId()))
                .setProjection(Projections.sum("amount"));
        return (Double) incomeLedgerAmountCriteria.uniqueResult();
    }

    public Double fetch(String foundsFlowTypeTitle) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Double> criteria = builder.createQuery(Double.class);
        Root<Ledger> root = criteria.from(Ledger.class);

        criteria.select(builder.sum(root.get(Ledger_.amount)))
                .where(builder.equal(root.get(Ledger_.foundsFlowsType), dao.findByTitle(foundsFlowTypeTitle)));
        return entityManager.createQuery(criteria).getSingleResult();
    }
}
