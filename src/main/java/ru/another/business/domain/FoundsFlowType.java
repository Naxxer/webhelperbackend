package ru.another.business.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Anton.Ilin
 *         date: 02.12.16.
 */

@Getter
@Setter
@Entity
@Table(name = "founds_flow_type")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class FoundsFlowType implements Serializable {

	@Id
	@Column
	@GeneratedValue(generator = "foundsFlowTypeIdGenerator")
	@GenericGenerator(
			name = "foundsFlowTypeIdGenerator",
			strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
			parameters = {
					@Parameter(name = "sequence_name", value = "founds_flow_type_id_seq"),
					@Parameter(name = "optimizer", value = "hilo"),
					@Parameter(name = "initial_value", value = "1"),
					@Parameter(name = "increment_size", value = "1")
			}
	)
	private Long id;

	@Column
	private Long code;

	@Column
	private String title;
}
