package ru.another.business.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Anton.Ilin
 *         date: 02.12.16.
 */

@Getter
@Setter
@Entity
@Table(name = "ledger")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Ledger implements Serializable {

	@Id
	@Column
	@GeneratedValue(generator = "ledgerIdGenerator")
	@GenericGenerator(
			name = "ledgerIdGenerator",
			strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
			parameters = {
					@Parameter(name = "sequence_name", value = "ledger_id_seq"),
					@Parameter(name = "optimizer", value = "hilo"),
					@Parameter(name = "initial_value", value = "1"),
					@Parameter(name = "increment_size", value = "1")
			}
	)
	private Long id;

	@Column(name = "dateOfOperation")
	private Date timestamp;

	@OneToOne
	@JoinColumn(name = "founds_flow_type_id")
	private FoundsFlowType foundsFlowsType;

	@Column
	private Double amount;
}
