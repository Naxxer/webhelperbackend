package ru.another.business.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Anton.Ilin
 *         date: 02.12.16.
 */
@Getter
@Setter
public class FoundsFlowTypeDto {

	Long id;
	Long code;
	String title;

	public FoundsFlowTypeDto() {}
}
