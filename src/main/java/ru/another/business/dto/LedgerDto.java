package ru.another.business.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author Anton.Ilin
 *         date: 02.12.16.
 */
@Getter
@Setter
public class LedgerDto {

	Long id;
	Date timestamp;
	FoundsFlowTypeDto foundsFlowsType;
	Double amount;

	public LedgerDto() {}

	@Override
	public String toString() {
		return "\nLedger:\nid: " + String.valueOf(id) + "\ndate: " + String.valueOf(timestamp) +
				"\n  Founds flow type:" + (foundsFlowsType == null ? " null" : "\n  " + String.valueOf(foundsFlowsType.getId()) + "\n  " + String.valueOf(foundsFlowsType.getCode()) + "\n  " + String.valueOf(foundsFlowsType.getTitle())) +
				"\namount: " + String.valueOf(amount);
	}
}
