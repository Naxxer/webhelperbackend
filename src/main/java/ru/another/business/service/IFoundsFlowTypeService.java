package ru.another.business.service;

import ru.another.business.dto.FoundsFlowTypeDto;

import java.util.List;

/**
 * @author Anton.Ilin
 *         date: 02.12.16.
 */
public interface IFoundsFlowTypeService {

	FoundsFlowTypeDto save(FoundsFlowTypeDto foundsFlowTypeDto);

	FoundsFlowTypeDto get(Long id);

	List<FoundsFlowTypeDto> getAll();

	void delete(Long id);
}
