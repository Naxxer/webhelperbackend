package ru.another.business.service;

import ru.another.business.dto.LedgerDto;

import java.util.List;

/**
 * @author Anton.Ilin
 *         date: 02.12.16.
 */
public interface ILedgerService {

	LedgerDto save(LedgerDto ledgerDto);

	LedgerDto get(Long id);

	List<LedgerDto> getAll();

	void delete(Long id);

	Double fetchBalance();
}
