package ru.another.business.service.implementation;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.another.business.dao.IFoundsFlowTypeDao;
import ru.another.business.domain.FoundsFlowType;
import ru.another.business.dto.FoundsFlowTypeDto;
import ru.another.business.service.IFoundsFlowTypeService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Anton.Ilin
 *         date: 02.12.16.
 */
@Service
@Transactional
public class FoundsFlowTypeService implements IFoundsFlowTypeService{

	private final Mapper mapper;
	private final IFoundsFlowTypeDao dao;

	@Autowired
	public FoundsFlowTypeService(Mapper mapper, IFoundsFlowTypeDao dao) {
		this.mapper = mapper;
		this.dao = dao;
	}

	@Override
	public FoundsFlowTypeDto save(FoundsFlowTypeDto foundsFlowTypeDto) {
		return mapper.map(dao.save(mapper.map(foundsFlowTypeDto, FoundsFlowType.class)), FoundsFlowTypeDto.class);
	}

	@Override
	public FoundsFlowTypeDto get(Long id) {
		return mapper.map(dao.findOne(id), FoundsFlowTypeDto.class);
	}

	@Override
	public List<FoundsFlowTypeDto> getAll() {
		List<FoundsFlowTypeDto> ledgerDtoList = new ArrayList<>();
		for (FoundsFlowType foundsFlowType : dao.findAll()) {
			ledgerDtoList.add(mapper.map(foundsFlowType, FoundsFlowTypeDto.class));
		}
		return ledgerDtoList;
	}

	@Override
	public void delete(Long id) {
		dao.delete(id);
	}
}
