package ru.another.business.service.implementation;

import lombok.extern.slf4j.Slf4j;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.another.business.dao.ILedgerDao;
import ru.another.business.domain.Ledger;
import ru.another.business.dto.LedgerDto;
import ru.another.business.service.ILedgerService;
import ru.another.business.utils.WebHelperUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Anton.Ilin
 *         date: 02.12.16.
 */
@Slf4j
@Service
@Transactional
public class LedgerService implements ILedgerService{

	private final Mapper mapper;
	private final ILedgerDao dao;

	@Autowired
	public LedgerService(Mapper mapper, ILedgerDao dao) {
		this.mapper = mapper;
		this.dao = dao;
	}

	@Override
	public LedgerDto save(LedgerDto ledgerDto) {
		return mapper.map(dao.save(mapper.map(ledgerDto, Ledger.class)), LedgerDto.class);
	}

	@Override
	public LedgerDto get(Long id) {
		return mapper.map(dao.findOne(id), LedgerDto.class);
	}

	@Override
	public List<LedgerDto> getAll() {
		List<LedgerDto> ledgerDtoList = new ArrayList<>();
		for (Ledger ledger : dao.findAll()) {
			ledgerDtoList.add(mapper.map(ledger, LedgerDto.class));
		}
		return ledgerDtoList;
	}

	@Override
	public void delete(Long id) {
		dao.delete(id);
	}

	@Override
	public Double fetchBalance() {
		Double income = dao.fetchFoundsFlowAmount(WebHelperUtils.FOUNDS_FLOW_TYPE_INCOME);
		Double outcome = dao.fetchFoundsFlowAmount(WebHelperUtils.FOUNDS_FLOW_TYPE_OUTCOME);
		if (income == null) {
			return -outcome;
		} else if (outcome == null) {
			return income;
		} else {
			return income - outcome;
		}
	}
}
