package ru.another.web.api.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.another.business.dto.FoundsFlowTypeDto;
import ru.another.business.service.IFoundsFlowTypeService;

import java.util.List;

/**
 * @author Anton.Ilin
 *         date: 02.12.16.
 */
@Slf4j
@RestController
@RequestMapping("/api/founds_flow")
public class FoundsFlowTypeApiController {

	private final IFoundsFlowTypeService service;

	@Autowired
	public FoundsFlowTypeApiController(IFoundsFlowTypeService service) {
		this.service = service;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping("/new")
	public ResponseEntity<FoundsFlowTypeDto> save(@RequestBody FoundsFlowTypeDto foundsFlowTypeDto) {
		log.info("{}", foundsFlowTypeDto.getCode());
		return new ResponseEntity<>(service.save(foundsFlowTypeDto), HttpStatus.OK);
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/{id}")
	public ResponseEntity<FoundsFlowTypeDto> get(@PathVariable Long id) {
		return new ResponseEntity<>(service.get(id), HttpStatus.OK);
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/all")
	public ResponseEntity<List<FoundsFlowTypeDto>> getAll() {
		log.info("Obtain all records");
		return new ResponseEntity<>(service.getAll(), HttpStatus.OK);
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@PutMapping("/update")
	public ResponseEntity<FoundsFlowTypeDto> update(@RequestBody FoundsFlowTypeDto foundsFlowTypeDto) {
		return new ResponseEntity<>(service.save(foundsFlowTypeDto), HttpStatus.OK);
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		service.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
