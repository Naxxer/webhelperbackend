package ru.another.web.api.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.another.business.dto.LedgerDto;
import ru.another.business.service.ILedgerService;

import java.util.List;

/**
 * @author Anton.Ilin
 *         date: 02.12.16.
 */
@Slf4j
@RestController
@RequestMapping("/api/ledger")
public class LedgerApiController {

	private final ILedgerService service;

	@Autowired
	public LedgerApiController(ILedgerService service) {
		this.service = service;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping("/new")
	public ResponseEntity<LedgerDto> save(@RequestBody LedgerDto ledgerDto) {
		log.info("Saving new ledger record");
		log.info("Ledger DTO: {}", ledgerDto.toString());
		return new ResponseEntity<>(service.save(ledgerDto), HttpStatus.OK);
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/{id}")
	public ResponseEntity<LedgerDto> get(@PathVariable Long id) {
		log.info("Obtain ledger record by id {}", id);
		return new ResponseEntity<>(service.get(id), HttpStatus.OK);
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/all")
	public ResponseEntity<List<LedgerDto>> getAll() {
		log.info("Obtain all records");
		return new ResponseEntity<>(service.getAll(), HttpStatus.OK);
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@PutMapping("/update")
	public ResponseEntity<LedgerDto> update(@RequestBody LedgerDto ledgerDto) {
		log.info("Updating ledger record with id {}", ledgerDto.getId());
		return new ResponseEntity<>(service.save(ledgerDto), HttpStatus.OK);
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		log.info("Removing ledger record with id {}", id);
		service.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/balance")
	public ResponseEntity<Double> getBalance() {
		log.info("Obtain current balance");
		return new ResponseEntity<>(service.fetchBalance(), HttpStatus.OK);
	}
}
