CREATE TABLE ledger (
  id                  BIGINT       NOT NULL PRIMARY KEY,
  date_of_operation   TIMESTAMP(6) NOT NULL,
  founds_flow_type_id BIGINT       NOT NULL,
  amount              FLOAT8       NOT NULL
);

CREATE TABLE founds_flow_type (
  id   BIGINT NOT NULL PRIMARY KEY,
  code BIGINT NOT NULL
);

ALTER TABLE ledger
  ADD CONSTRAINT founds_flow_type_id_fk FOREIGN KEY (founds_flow_type_id) REFERENCES founds_flow_type (id);
